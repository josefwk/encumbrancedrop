
function handleItemDropRequest(request)
    item = request["object"]
    player = request["player"]
    cellName = ""
    if player.cell ~= nil then cellName = player.cell.name end
    
    item:teleport(cellName, player.position)
end

return {
    eventHandlers = {
        onItemDropRequested = handleItemDropRequest,
    }
}