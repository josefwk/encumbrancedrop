local types = require('openmw.types')
local self = require('openmw.self')
local core = require('openmw.core')

function checkEncumbrance()
    maxCapacity = types.NPC.getCapacity(self)
    currentWeight = types.Actor.getEncumbrance(self)
    if currentWeight > maxCapacity then
        print("Overencumbered at " .. tostring(currentWeight) .. "/" .. tostring(maxCapacity) .. ".")
        dropItems()
    end
end

function dropItems()
    print("Dropping all non-Book items.")
    playerInventory = types.Actor.inventory(self):getAll()
    for _, item in pairs(playerInventory) do
        if item.type ~= types.Book then
            eventData = {
                ["object"] = item,
                ["player"] = self,
            }
            core.sendGlobalEvent('onItemDropRequested', eventData)
        end
    end
end

return {
    engineHandlers = {
        onUpdate = checkEncumbrance
    }
}